// Filename: spectraph_graph.js
// Timestamp: 2018.05.07-12:59:10 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import { Map } from 'immutable'

import {
  sphfs_getchildkey,
  sphfs_getparentnode
} from './sphfs.js'

import {
  sphedge_isvalidname
} from './sphedge.js'

import {
  sphnode_keyformat,
  sphnode_getchildarr,
  sphnode_getdatainarr,
  sphnode_getdataoutarr,
  sphnode_getfromjs,
  sphnode_setedgein,
  sphnode_setedgeout,
  sphnode_rmedgeout,
  sphnode_rmedgein,
  sphnode_getrefnameoutedge,
  sphnode_getrefnameinedge
} from './sphnode.js'

import {
  sphenumMETA as META,
  sphenumNS as NS,
  sphenumTYPE as TYPE,
  sphenumKEY as KEY,
  sphenumNAME as NAME,
  sphenumREFNAME as REFNAME,
  sphenumDATAINARR as DATAINARR,
  sphenumCHILDARR as CHILDARR,
  sphenumEDGEINDISABLE,
  sphenumEDGEOUTDISABLE
} from './sphenum.js'

const sphgraph_get = () => Map({})

const sphgraph_is = graph =>
  Map.isMap(graph) && graph.has('/')

// get node from js/json
const sphgraph_getfromjs = js => (
  js && Object.keys(js).reduce((gr, key) => (
    gr.set(js[key].key, sphnode_getfromjs(js[key]))), sphgraph_get()))

// set cnode as child on pnode, w/ given name
const sphgraph_setnodechild = (gr, pnode, cnode, name) => {
  let cname = sphnode_keyformat(name || cnode.get(NAME)),
      ckey = sphfs_getchildkey(pnode, cname),
      cnodefin = cnode.set(KEY, ckey)
  // console.log(pnode.toJS())
  let pnodefin = pnode.set(
        CHILDARR, sphnode_getchildarr(pnode).push(ckey)),
      graphfin = gr.set(ckey, cnodefin)

  if (!gr.has(pnode.get(KEY))) {
    throw new Error(`pnode not found ${pnode.get(KEY)}`)
  }

  return [
    graphfin.set(pnodefin.get(KEY), pnodefin),
    pnodefin,
    cnodefin
  ]
}

const sphgraph_setnode = (gr, node) => (
  [gr.set(node.get(KEY), node), node])

const sphgraph_getnode = (gr, node) => (
  gr.get(typeof node === 'string' ? node : node.get(KEY)))

// return first node with key pattern matching a root pattern
//
//   /rootnode
//   /not/root/node
const sphgraph_getnoderoot = (gr, rootre = /^[^/]*\/[^/]*$/) => (
  gr.find((val, key) => rootre.test(key)) || null)


// construct useful edge id serializing relationship
//
// [/root-content/addtodo/selectall].subj.total
//    ⸺> [/datausers].subj.childlength
//
const sphgraph_getedgeid = (outkey, outns, outname, infull) => (
  outname
    ? `[${outkey}].${outns}.${outname} ⸺> ${infull}`
    : `[${outkey}].${outns} ⸺> ${infull}`)

// meta is stored on 'out' edge only
// eslint-disable-next-line max-len
const sphgraph_setnodeedge = (gr, innode, outnode, refname, type, ns, meta, comparetype) => {
  let innodekey = innode.get(KEY),
      outnodekey = outnode.get(KEY),
      edgeid = sphgraph_getedgeid(outnodekey, ns, meta.name, meta.fullstr)

  if (gr.has(outnodekey) && gr.has(innodekey)) {
    // rather than existing graph nodes, make param nodes canonical
    gr = gr
      .set(outnodekey, outnode)
      .set(innodekey, innode)
    gr = meta[sphenumEDGEINDISABLE] ? gr : gr
      .set(outnodekey, sphnode_setedgein(
        edgeid,
        outnode,
        innodekey,
        refname, type, null, null, comparetype))
    gr = meta[sphenumEDGEOUTDISABLE] ? gr : gr
      .set(innodekey, sphnode_setedgeout(
        edgeid,
        gr.get(innodekey),
        outnodekey,
        refname, type, ns, meta, comparetype))
  } else {
    console.error(gr.toJS())
    throw new Error(`[!!!] edge node not found in graph: ${(
      gr.has(outnodekey)
        ? innodekey
        : outnodekey
    )}`)
  }

  return [
    gr,
    gr.get(innodekey),
    gr.get(outnodekey)
  ]
}

// redefines incoming and outgoing edge nodes to link directly to one another
// in place of 'removed' node.
//
// innode => node => outnode
//
// becomes: innode => outnode
const sphgraph_rmedgenode = (gr, nd, refname) => {
  let edgein = sphnode_getrefnameinedge(nd, refname),
      edgeinkey = edgein && edgein.get(KEY),
      nodein,
      edgeout = sphnode_getrefnameoutedge(nd, refname),
      edgeoutkey = edgeout && edgeout.get(KEY),
      nodeout,
      nodekey

  if (gr.has(edgeinkey)) {
    nodekey = nd.get(KEY)
    nodein = sphnode_rmedgeout(gr.get(edgeinkey), nodekey)
    nd = sphnode_rmedgein(nd, edgeinkey)
    gr = gr
      .set(edgeinkey, nodein)
      .set(nodekey, nd)
  }

  if (gr.has(edgeoutkey)) {
    nodekey = nd.get(KEY)
    nodeout = sphnode_rmedgeout(gr.get(edgeoutkey), nodekey)
    nd = sphnode_rmedgeout(nd, edgeoutkey)
    gr = gr
      .set(edgeoutkey, nodeout)
      .set(nodekey, nd)
  }

  if (nodeout && nodein) {
    const nodeedgeres = sphgraph_setnodeedge(
      gr,
      nodein,
      nodeout,
      refname,
      edgeout.get(TYPE),
      edgeout.get(NS),
      edgeout.get(META))

    gr = nodeedgeres[0]
    nodein = nodeedgeres[1]
    nodeout = nodeedgeres[2]
  }

  return [gr, nd]
}

// 'insert' the node within the edge relationship
//
// original edge from 'edgenode':
//   **edgenode** --refname--> outnode
//
// modified edge from 'edgenode':
//   **edgenode** --refname--> node --refname--> outnode
//
const sphgraph_setnodeinedge = (gr, edgenode, edgename, node) => {
  let outnodeedge = sphnode_getrefnameoutedge(edgenode, edgename),
      outnode = gr.get(outnodeedge.get(KEY)),
      inedge = sphnode_getrefnameinedge(outnode, edgename),
      setnodeedgeres,
      meta = outnodeedge.get(META),
      type = outnodeedge.get(TYPE),
      ns = outnodeedge.get(NS)

  edgenode = sphnode_rmedgeout(edgenode, outnodeedge.get(KEY))
  outnode = sphnode_rmedgein(outnode, inedge.get(KEY))

  gr = gr
    .set(edgenode.get(KEY), edgenode)
    .set(outnode.get(KEY), outnode)

  setnodeedgeres = sphgraph_setnodeedge(
    gr, edgenode, node, edgename, type, ns, meta)
  gr = setnodeedgeres[0]
  edgenode = setnodeedgeres[1]
  node = setnodeedgeres[2]

  // ;[gr, node, outnode]
  setnodeedgeres = sphgraph_setnodeedge(
    gr, node, outnode, edgename, type, ns, meta)
  gr = setnodeedgeres[0]

  return [gr, edgenode]
}

// 'insert' the node within the edge relationship
//
// original edge from 'edgenode':
//   innode --refname--> **edgenode**
//
// modified edge from 'edgenode':
//   innode --refname--> node --refname-->  **edgenode**
//
const sphgraph_setnodeoutedge = (gr, edgenode, edgename, node) => {
  let setnodeedgeres,
      innodeedge = sphnode_getrefnameinedge(edgenode, edgename),
      innode = gr.get(innodeedge.get(KEY)),
      outedge = sphnode_getrefnameoutedge(innode, edgename),

      meta = outedge.get(META),
      type = outedge.get(TYPE),
      ns = outedge.get(NS)

  edgenode = sphnode_rmedgein(edgenode, innodeedge.get(KEY))
  innode = sphnode_rmedgeout(innode, outedge.get(KEY))

  gr = gr
    .set(edgenode.get(KEY), edgenode)
    .set(innode.get(KEY), innode)

  setnodeedgeres = sphgraph_setnodeedge(
    gr, node, edgenode, edgename, type, ns, meta)
  gr = setnodeedgeres[0]
  node = setnodeedgeres[1]
  edgenode = setnodeedgeres[2]

  // [gr, innode, node]
  setnodeedgeres = sphgraph_setnodeedge(
    gr, innode, node, edgename, type, ns, meta)
  gr = setnodeedgeres[0]
  innode = setnodeedgeres[1]
  node = setnodeedgeres[2]

  return [gr, edgenode, node]
}

// eslint-disable-next-line max-len
const sphgraph_getdeparrall = (gr, nd = sphgraph_getnoderoot(gr), opts = {}, narr = [], parr = []) => {
  if (!nd)
    return narr

  if (!narr.some(elem =>
    elem.get(KEY) === nd.get(KEY)
  ) /* && !parr.some(function (pnode) {
         if (pnode.get(KEY) === node.get(KEY)) {
         if (opts.iscircular === false) {
         throw new Error('[!!!] circular dependency: :pnode <=> :cnode'
         .replace(/:pnode/, pnode.get(KEY))
         .replace(/:cnode/, parr[0].get(KEY)));
         }
         return true;
         }
         })) { */) {
    //  parr.unshift(node);
    narr.push(nd)
    sphnode_getdataoutarr(nd).map(edge => {
      narr = sphgraph_getdeparrall(gr, gr.get(edge.get(KEY)), opts, narr, parr)
    })
  }

  return narr
}

const sphgraph_getdeparr = (gr, nd, opts) => (
  sphgraph_getdeparrall(gr, nd, opts).slice(1))

const sphgraph_walkedgeslist = (gr, ndsrc, edgename, edgeslist, prefn, fn) => {
  const edge = edgeslist.first()

  if (!edge)
    return fn(null, gr, gr.get(ndsrc.get(KEY)))

  return (edgename && edge.get(REFNAME) !== edgename)
    ? sphgraph_walkedgeslist(gr, ndsrc, edgename, edgeslist.rest(), prefn, fn)
    : prefn(gr, ndsrc, edge, (e, gr) => {
      if (e) return fn(e)

      sphgraph_walkedgeslist(gr, ndsrc, edgename, edgeslist.rest(), prefn, fn)
    })
}

const sphgraph_walkedges = (gr, ndsrc, edgename, prefn, fn) => {
  const outedgearr = sphnode_getdataoutarr(ndsrc)

  edgename = sphedge_isvalidname(edgename) && edgename

  sphgraph_walkedgeslist(gr, ndsrc, edgename, outedgearr, prefn, fn)
}

const sphgraph_followedge = (gr, ndsrc, edgename, fn, prefn) => {
  const edgeoutarr = sphnode_getdataoutarr(ndsrc)
  const entry = edgeoutarr.findEntry(edge => edge.get(REFNAME) === edgename)
  const edge = entry && entry[1]

  if (!edge) {
    return fn(null, gr, gr.get(ndsrc.get(KEY)))
  }

  prefn(gr, gr.get(edge.get(KEY)), edge, (e, gr, edgenode) => {
    if (e) return fn(e)

    sphgraph_followedge(gr, edgenode, edgename, fn, prefn)
  })
}

const sphgraph_rmedges = (gr, nd) => {
  const ndkey = nd.get(KEY)

  nd = nd.set(DATAINARR, sphnode_getdatainarr(nd).filter(edge => {
    const edgekey = edge.get(KEY)

    if (gr.has(edgekey))
      ([gr] = sphgraph_setnode(gr, (
        sphnode_rmedgeout(gr.get(edgekey), ndkey))))
  }))

  return [gr, nd]
}

// remove,
//  * edges
//  * refs from parent
//  * key and def from graph
const sphgraph_rmnode = (gr, nd) => {
  let res
  const nodekey = nd.get(KEY)
  const pnode = sphfs_getparentnode(gr, nd)


  res = sphgraph_rmedges(gr, nd)
  gr = res[0]
  nd = res[1]
  res = sphgraph_setnode(gr, pnode.set(CHILDARR, (
    sphnode_getchildarr(pnode).filter(key => key !== nodekey))))
  gr = res[0]

  return [gr.delete(nodekey), nd]
}

const sphgraph_getparent = (gr, nd) => (
  sphfs_getparentnode(gr, nd))

const sphgraph_getsiblingkeyprev = (gr, nd) => {
  let siblings = sphnode_getchildarr(sphgraph_getparent(gr, nd)),
      nodeindex = siblings.indexOf(nd.get(KEY))

  return nodeindex > 0
    ? siblings.get(--nodeindex) || null
    : null
}

const sphgraph_getsiblingprev = (gr, nd) => {
  const key = sphgraph_getsiblingkeyprev(gr, nd)
  const sibling = key ? gr.get(key) : null

  return sibling
}

const sphgraph_getsiblingkeynext = (gr, nd) => {
  let siblings = sphnode_getchildarr(sphgraph_getparent(gr, nd)),
      nodeindex = siblings.indexOf(nd.get(KEY))

  return nodeindex !== -1 && (nodeindex < siblings.count())
    ? siblings.get(++nodeindex) || null
    : null
}

const sphgraph_getsiblingnext = (gr, nd) => {
  const key = sphgraph_getsiblingkeynext(gr, nd)
  const sibling = key ? gr.get(key) : null

  return sibling
}

// eslint-disable-next-line max-len
const sphgraph_dokeychildslist = (gr, list, dofn, fn, i = 0, finding = false) => {
  const keychild = list.first()

  if (!keychild)
    return fn(null, gr)

  dofn(gr, keychild, i, (e, gr, nd, istrue) => {
    if (e) return fn(e)

    if (finding && istrue) {
      return fn(null, gr, nd)
    }

    sphgraph_dokeychildslist(gr, list.rest(), dofn, fn, ++i, finding)
  })
}

const sphgraph_dokeychilds = (gr, nd, dofn, fn) => (
  sphgraph_dokeychildslist(gr, sphnode_getchildarr(nd), dofn, (e, gr) => (
    fn(e, gr, gr.get(nd.get(KEY))))))

const sphgraph_findkeychild = (gr, nd, dofn, fn) => (
  sphgraph_dokeychildslist(gr, sphnode_getchildarr(nd), dofn, fn, 0, true))

export {
  sphgraph_is,
  sphgraph_get,
  sphgraph_getfromjs,
  sphgraph_setnodechild,
  sphgraph_setnode,
  sphgraph_getnode,
  sphgraph_getnoderoot,
  sphgraph_getedgeid,
  sphgraph_setnodeedge,
  sphgraph_rmedgenode,
  sphgraph_setnodeinedge,
  sphgraph_setnodeoutedge,
  sphgraph_getdeparrall,
  sphgraph_getdeparr,
  sphgraph_walkedgeslist,
  sphgraph_walkedges,
  sphgraph_followedge,
  sphgraph_rmedges,
  sphgraph_rmnode,
  sphgraph_getparent,
  sphgraph_getsiblingkeyprev,
  sphgraph_getsiblingprev,
  sphgraph_getsiblingkeynext,
  sphgraph_getsiblingnext,
  sphgraph_dokeychildslist,
  sphgraph_dokeychilds,
  sphgraph_findkeychild
}

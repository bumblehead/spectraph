// Filename: spectraph_edge.js
// Timestamp: 2017.02.24-14:47:54 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import { Map } from 'immutable'

import {
  sphenumID as ID,
  sphenumNS as NS,
  sphenumKEY as KEY,
  sphenumMETA as META,
  sphenumREFNAME as REFNAME,
  sphenumTYPE as TYPE
} from './sphenum.js'

const sphedge_get = (id, refname, type, key, ns, meta) => Map(Object.assign(
  meta ? { [META]: meta } : {},
  ns ? { [NS]: ns } : {},
  {
    [ID]: id,
    [TYPE]: type || refname,
    [REFNAME]: refname,
    [KEY]: key
  }))

const sphedge_getfromjs = jsobj => sphedge_get(
  jsobj[ID], jsobj[REFNAME], jsobj[TYPE],
  jsobj[KEY], jsobj[NS], jsobj[META])

const sphedge_isvalidname = name => {
  const type = typeof name

  return Boolean(type === 'number' ||
    (type === 'string' && name.length))
}

const sphedge_issame = (edgea, edgeb) => (
  edgea.get(ID) === edgeb.get(ID))

const sphedge_issamenot = (edgea, edgeb) => (
  !sphedge_issame(edgea, edgeb))

export {
  sphedge_get,
  sphedge_getfromjs,
  sphedge_isvalidname,
  sphedge_issame,
  sphedge_issamenot
}

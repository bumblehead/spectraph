// Filename: spectraph_fs.js
// Timestamp: 2017.07.09-19:51:59 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import {
  sphenumKEY as KEY
} from './sphenum.js'

const dummydomain = new URL('http://dummy.com')
const endwordre = /[ /.]*$/ // matches end '/' @ /my/full/path/
const slcharcode = 47

// was: 'pathjoin(node.get(KEY), childname))'
const pathjoin = (basepath, subpath) => {
  const basepathdir = basepath === '/' ? basepath : basepath + '/'
  const pathname = new URL(
    './' + subpath, new URL(basepathdir, dummydomain)).pathname

  return (basepath.charCodeAt(0) === slcharcode)
    ? pathname
    : pathname.slice(1)
}

const sphfs_getchildkey = (node, childname) => (
  pathjoin(node.get(KEY), childname))

const sphfs_getrootkey = node => (
  pathjoin('/', node.get(KEY)))

// 'canvas/nav/links/thumb' ./img := canvas/nav/links/img
const sphfs_getrelpath = (node, path) => {
  // if path ls /root/path... returns root
  if (typeof path !== 'string') {
    throw Error('path must be string value')
  } else if (path.charCodeAt(0) === slcharcode) {
    return path
  } else if (path === './') {
    path = node.get(KEY)
  } else {
    path = pathjoin(
      node.get(KEY).replace(endwordre, ''), path).replace(endwordre, '')
  }

  return path
}

const sphfs_getrelnodepath = (graph, nodecwd, path) => (
  graph.get(sphfs_getrelpath(nodecwd, path)))

const sphfs_getrelnode = (graph, node, path) => (
  path === './' ? node : graph.get(sphfs_getrelpath(node, path)))

// return '/path/to' from '/path/to/node'
//
// if removing the 'last' path leaves an empty string ('/path' => '')
// return root path '/'
//
const sphfs_lastpathre = /\/[^/]*$/
const sphfs_getparentdir = path => (
  path.replace(sphfs_lastpathre, '') || '/')

const sphfs_getparentnode = (graph, node) => (
  graph.get(sphfs_getparentdir(node.get(KEY))))

export {
  sphfs_getchildkey,
  sphfs_getrootkey,
  sphfs_getrelpath,
  sphfs_getrelnodepath,
  sphfs_getrelnode,
  sphfs_getparentdir,
  sphfs_getparentnode
}

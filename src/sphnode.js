
// Filename: spectraph_node.js
// Timestamp: 2017.07.17-00:46:55 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import { List, Map, is } from 'immutable'

import {
  sphedge_get,
  sphedge_getfromjs,
  sphedge_issamenot
} from './sphedge.js'

import {
  sphenumKEY as KEY,
  sphenumEDGE as EDGE,
  sphenumMETA as META,
  sphenumREFNAME as REFNAME,
  sphenumDATAINARR as DATAINARR,
  sphenumDATAOUTARR as DATAOUTARR,
  sphenumCHILDARR as CHILDARR
} from './sphenum.js'

const IMLIST = List()
const spacesre = /\s/g

const sphnode_keyformat = str => str.replace(spacesre, '-')

// graph-like nodes with childarr _references_ to other node definitions
const sphnode_get = (spec, name, key) => (
  Map(spec).merge(
    Map({
      name,
      key: sphnode_keyformat(key || name)
      //
      // other elements defined when populated,
      //
      // childarr : List()
      // datainarr : List(),
      // dataoutarr : List()
    })))

const sphnode_getchildarr = node => (
  node.get(CHILDARR, IMLIST))

const sphnode_getdatainarr = node => (
  node.get(DATAINARR, IMLIST))

const sphnode_getdataoutarr = node => (
  node.get(DATAOUTARR, IMLIST))

const sphnode_getas = (spec, name = spec.name, key = name) => (
  sphnode_get(spec, name, sphnode_keyformat(key)))

const sphnode_assign = (node, spec) => (
  node.merge(Map(spec)))

const sphnode_is = node => (
  Map.isMap(node) && node.get(KEY))

const sphnode_issame = (nodea, nodeb) => (
  is(nodea, nodeb))

// return [node, childindex]
const sphnode_rmchildkey = (node, childkey, childindex = null) => [
  node.set(CHILDARR, sphnode_getchildarr(node).filter((key, i) => (
    key !== childkey || (childindex = i, false)
  ))),
  childindex
]

const sphnode_mvarrelem = (arr, newi, oldi) => (
  oldi = typeof oldi === 'number' ? oldi : arr.length - 1,
  arr.splice(newi, 0, arr.splice(oldi, 1)[0]),
  arr)

const sphnode_mvlistelem = (list, newi, oldi) => (
  oldi = typeof oldi === 'number' ? oldi : list.size - 1,
  list.delete(oldi).insert(newi, list.get(oldi)))

const sphnode_resetchilds = node => node
  .merge(Map({ childarr: List() }))

const sphnode_getfromjs = js => (
  Map(js).merge(
    Map({
      childarr: List(js.childarr),
      dataoutarr: List((js.dataoutarr || []).map(
        sphedge_getfromjs)),
      datainarr: List((js.datainarr || []).map(
        sphedge_getfromjs))
    })))

const sphnode_getcomparefn = (edge, comparetype = EDGE) => {
  let comparefn

  if (comparetype === EDGE) {
    comparefn = comparisonedge => (
      sphedge_issamenot(edge, comparisonedge))
  } else if (comparetype === REFNAME) {
    comparefn = comparisonedge => (
      edge.get(comparetype) !== comparisonedge.get(comparetype))
  }

  return comparefn
}

// comparetype EDGE,
//   overwrite existing edges matching given edge
//
// comparetype REFNAME,
//   overwrite existing edges w/ refname matching given edge,
//
// eslint-disable-next-line max-len
const sphnode_setedge = (id, node, key, refname, edgename, type, ns, meta, comparetype) => {
  const edge = sphedge_get(id, refname, type, key, ns, meta)
  const comparefn = sphnode_getcomparefn(edge, comparetype)

  return node.set(edgename, (
    node.get(edgename, IMLIST).filter(comparefn).push(edge)))
}

const sphnode_setedgeref = (node, key, refname, edgename, meta) => {
  const edge = sphedge_get(refname, key)

  return node.set(edgename, node.get(edgename, IMLIST).filter(inedge => (
    inedge.get(REFNAME) !== refname
  )).push(meta ? edge.set(META, meta) : edge))
}

// eslint-disable-next-line max-len
const sphnode_setedgein = (id, node, key, refname, type, ns, meta, comparetype) => (
  // eslint-disable-next-line max-len
  sphnode_setedge(id, node, key, refname, DATAINARR, type, ns, meta, comparetype))

// eslint-disable-next-line max-len
const sphnode_setedgeout = (id, node, key, refname, type, ns, meta, comparetype) => (
  // eslint-disable-next-line max-len
  sphnode_setedge(id, node, key, refname, DATAOUTARR, type, ns, meta, comparetype))

// type is optional
const sphnode_rmedgeout = (node, key) => (
  node.set(DATAOUTARR, sphnode_getdataoutarr(node).filter(edge => (
    edge.get(KEY) !== key
  ))))

const sphnode_rmedgein = (node, key) => (
  node.set(DATAINARR, sphnode_getdatainarr(node).filter(edge => (
    edge.get(KEY) !== key
  ))))

const sphnode_getrefnameoutedge = (node, refname) => (
  sphnode_getdataoutarr(node).find(edge => edge.get(REFNAME) === refname))

const sphnode_getrefnameoutedgenode = (traph, node, refname) => (
  traph.get(sphnode_getrefnameoutedge(node, refname).get(KEY)))

const sphnode_getrefnameinedge = (node, refname) => (
  sphnode_getdatainarr(node).find(edge => edge.get(REFNAME) === refname))

const sphnode_getrefnameinedgenode = (traph, node, refname) => (
  traph.get(sphnode_getrefnameinedge(node, refname).get(KEY)))

export {
  sphnode_keyformat,
  sphnode_get,
  sphnode_getchildarr,
  sphnode_getdatainarr,
  sphnode_getdataoutarr,
  sphnode_getas,
  sphnode_assign,
  sphnode_is,
  sphnode_issame,
  sphnode_rmchildkey,
  sphnode_mvarrelem,
  sphnode_mvlistelem,
  sphnode_resetchilds,
  sphnode_getfromjs,
  sphnode_getcomparefn,
  sphnode_setedge,
  sphnode_setedgeref,
  sphnode_setedgein,
  sphnode_setedgeout,
  sphnode_rmedgeout,
  sphnode_rmedgein,
  sphnode_getrefnameoutedge,
  sphnode_getrefnameoutedgenode,
  sphnode_getrefnameinedge,
  sphnode_getrefnameinedgenode
}

// Filename: spectraph_token.js
// Timestamp: 2017.07.09-19:57:04 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const sphenumID = 'id'
const sphenumNS = 'ns'
const sphenumKEY = 'key'
const sphenumNAME = 'name'
const sphenumEDGE = 'edge'
const sphenumMETA = 'meta'
const sphenumREFNAME = 'refname'
const sphenumDATAINARR = 'datainarr'
const sphenumDATAOUTARR = 'dataoutarr'
const sphenumCHILDARR = 'childarr'
const sphenumTYPE = 'gn:type'

const sphenumEDGEINDISABLE = 'edgein:disable'
const sphenumEDGEOUTDISABLE = 'edgeout:disable'

export {
  sphenumID,
  sphenumNS,
  sphenumKEY,
  sphenumNAME,
  sphenumEDGE,
  sphenumMETA,
  sphenumREFNAME,
  sphenumDATAINARR,
  sphenumDATAOUTARR,
  sphenumCHILDARR,

  sphenumEDGEINDISABLE,
  sphenumEDGEOUTDISABLE,

  sphenumTYPE
}

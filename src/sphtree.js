// Filename: spectraph_tree.js
// Timestamp: 2017.02.25-01:26:01 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import {
  sphenumKEY as KEY,
  sphenumDATAOUTARR as DATAOUTARR,
  sphenumCHILDARR as CHILDARR
} from './sphenum.js'

import {
  sphgraph_getnoderoot
} from './sphgraph.js'

const sphtree_get = (key, childarr) => ({
  key,
  childarr: childarr || []
})

// construct a tree through list keys define on given refname
//
// full tree impossible when circular reference
//
const sphtree_getfromref = (graph, gnode, refname, keyarr) => {
  gnode = gnode || sphgraph_getnoderoot(graph)
  keyarr = keyarr || []

  return gnode && sphtree_get(
    gnode.get(KEY),
    gnode.get(refname).map(nodekey => {
      if (keyarr.indexOf(nodekey) !== -1) {
        return sphtree_get(nodekey)
      }

      keyarr.push(nodekey)

      return sphtree_getfromref(graph, graph.get(nodekey), CHILDARR)
    }).toJS()
  )
}

const sphtree_getfromchild = (graph, gnode) => (
  sphtree_getfromref(graph, gnode, CHILDARR))

const sphtree_getfromdep = (graph, gnode) => (
  sphtree_getfromref(graph, gnode, DATAOUTARR))

// const sphtree_filtered = (tree, arr) => {
//   if (arr.indexOf(tree.key) !== -1)
//     return null
//
//   arr.push(tree.key)
//
//   return sphtree_get(
//    tree.key,
//    tree.nodes
//      .map(node => sphtree_filtered(node, arr))
//      .filter(n => n !== undefined)
//  )
// }

export {
  sphtree_get,
  sphtree_getfromref,
  sphtree_getfromchild,
  sphtree_getfromdep
}

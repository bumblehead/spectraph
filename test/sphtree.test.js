// Filename: spectraph.tree.spec.js
// Timestamp: 2017.02.25-01:43:45 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import test from 'node:test'
import assert from 'node:assert/strict'

import {
  sphgraph_getfromjs,
  sphgraph_getnoderoot,
  sphgraph_setnodechild,
  sphgraph_getnode
} from '../src/sphgraph.js'

import {
  sphnode_getfromjs
} from '../src/sphnode.js'

import {
  sphtree_get,
  sphtree_getfromref
  // sphtree_filtered
} from '../src/sphtree.js'

test('get should return a tree node', () => {
  const tree = sphtree_get('key')

  assert.deepStrictEqual(tree, {
    key: 'key',
    childarr: []
  })
})

test('getfromref should return a subtree at refname', () => {
  const graph = sphgraph_getfromjs({
    canvas: {
      key: 'canvas',
      childarr: [
        'canvas/nav'
      ]
    },
    'canvas/nav': {
      key: 'canvas/nav',
      childarr: [
        'canvas/nav/links'
      ]
    },
    'canvas/nav/links': {
      key: 'canvas/nav/links',
      childarr: [
        'canvas/nav/links/img',
        'canvas/nav/links/thumb'
      ],
      datainarr: [{
        refname: 'thumb',
        key: 'canvas/nav/links/thumb'
      }, {
        refname: 'img',
        key: 'canvas/nav/links/img'
      }]
    },
    'canvas/nav/links/thumb': {
      key: 'canvas/nav/links/thumb',
      dataoutarr: [{
        refname: 'links',
        key: 'canvas/nav/links'
      }]
    },
    'canvas/nav/links/img': {
      key: 'canvas/nav/links/img',
      dataoutarr: [{
        refname: 'links',
        key: 'canvas/nav/links'
      }]
    }
  })

  const gnode = sphgraph_getnode(graph, 'canvas')
  const reftree = sphtree_getfromref(
    graph, gnode, 'childarr')

  assert.deepStrictEqual(reftree, {
    key: "canvas",
    childarr: [{
      key: "canvas/nav",
      childarr: [{
        key: "canvas/nav/links",
        childarr: [{
          key: "canvas/nav/links/img",
          childarr: []
        }, {
          key: "canvas/nav/links/thumb",
          childarr: []
        }]
      }]
    }]
  })
})
/*
test('iltered should return tree filtered', () => {
  const reftree = {
    key: "canvas",
    nodes: [{
      key: "canvas/nav",
      nodes: [{
        key: "canvas/nav/links",
        nodes: [{
          key: "canvas/nav/links/img",
          nodes: []
        }, {
          key: "canvas/nav/links/thumb",
          nodes: []
        }]
      }]
    }]
  }

  const filtertree = sphtree_filtered(reftree, [])
})
*/
test('should have a test', async () => {
  const ext_nodes = {}
  let traph = sphgraph_getfromjs({
    '/canvas': {
      key: '/canvas',
      name: 'canvas',
      spec: {},
      newnodenames: [
        'body',
        'foot'
      ],
      childarr: [
        '/canvas/body'
      ],
      datainarr: [],
      dataoutarr: []
    }
  })

  ext_nodes.body = sphnode_getfromjs({
    key: '/canvas/body',
    name: 'body',
    spec: {},
    newnodenames: [
      'content'
    ],
    childarr: [
    ],
    datainarr: [],
    dataoutarr: []
  })

  ext_nodes.content = sphnode_getfromjs({
    key: '/canvas/body/content',
    name: 'content',
    spec: {},
    newnodenames: [
      'content_deep'
    ],
    childarr: [
    ],
    datainarr: [],
    dataoutarr: []
  })

  ext_nodes.content_deep = sphnode_getfromjs({
    key: '/canvas/body/content/deep',
    name: 'deep',
    spec: {},
    newnodenames: [],
    childarr: [
    ],
    datainarr: [],
    dataoutarr: []
  })

  ext_nodes.foot = sphnode_getfromjs({
    key: '/canvas/foot',
    name: 'foot',
    spec: {},
    newnodenames: [],
    childarr: [
      //  '/canvas/nav/links'
    ],
    datainarr: [],
    dataoutarr: []
  })

  const rootnode = sphgraph_getnoderoot(traph)
  const [traphn, noden] = await new Promise((resolve, error) => {
    (function dfs ([traph, node]) {
      let newnodenames = node.get('newnodenames')

      ;(function next (x, len, nodenames, traph, pnode/*, newnode*/) {
        if (x >= len) return resolve([traph, pnode]) // no childs --exit

        const [newtraph, , newcnode] = sphgraph_setnodechild(
          traph, pnode, ext_nodes[nodenames[x]])

        ////////////////////////////////////
        // preprocess here
        ////////////////////////////////////
        dfs([newtraph, newcnode], (err, [traph/*, node*/]) => {
          if (err) return error(err)

          ////////////////////////////////////
          // postprocess here
          ////////////////////////////////////
          next(++x, len, nodenames, traph, pnode)
        })
      }(0, newnodenames.length, newnodenames, traph, node))
    })([traph, rootnode])
  })

  // was 5 but updating tests only returns 4
  assert.strictEqual(traphn.count(), 4)
  assert.ok(noden)
  // assert.strictEqual( traphn.count(), 5 );
})

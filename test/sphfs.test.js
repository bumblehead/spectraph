import test from 'node:test'
import assert from 'node:assert/strict'

import {
  sphnode_get
} from '../src/sphnode.js'

import {
  sphgraph_getfromjs,
  sphgraph_getnode
} from '../src/sphgraph.js'

import {
  sphfs_getchildkey,
  sphfs_getrelpath,
  sphfs_getrelnode,
  sphfs_getparentdir,
  sphfs_getparentnode
} from '../src/sphfs.js'

import {
  mock_graph_todo_json,
  mock_graph_parentchildsimple_json
} from './mock/mockspecs.js'

test("should return the correct patharr from a node (../relative/path)", () => {
  const thumbkey = 'canvas/nav/links/thumb'
  const thumbnode = sphnode_get({}, 'thumb', thumbkey)

  assert.strictEqual(
    sphfs_getrelpath(thumbnode, '../relative/path'),
    'canvas/nav/links/relative/path')
})

test("should return the correct patharr from a node (./relative/path)", () => {
  let thumbkey = 'canvas/nav/links/thumb',
      thumbnode = sphnode_get({}, 'thumb',  thumbkey)

  assert.strictEqual(
    sphfs_getrelpath(thumbnode, './relative/path'),
    'canvas/nav/links/thumb/relative/path')
})

test("should return the correct patharr from a node (/relative/path)", () => {
  let thumbkey = 'canvas/nav/links/thumb',
      thumbnode = sphnode_get({}, 'thumb',  thumbkey)

  assert.strictEqual(
    sphfs_getrelpath(thumbnode, '/relative/path'),
    '/relative/path')
})

test("should return the path relativenode to the given node", () => {
  let graph_todo = sphgraph_getfromjs(mock_graph_todo_json),
      cwd_key = '/todo/content/box/listusers/2_user_id3',
      cwd_node = sphgraph_getnode(graph_todo, cwd_key),
      img_node = sphfs_getrelnode(
        graph_todo, cwd_node, '../../../box/adduser')

  assert.strictEqual(img_node.get('name'), 'adduser')
})

test("should return the parent path `/parent` from `/parent/child`", () => {
  assert.strictEqual(
    sphfs_getparentdir('/parent/child'),
    '/parent')
})

test("should return node `/parent` when given node `/parent/child`", () => {
  let graph_parentchildsimple =
      sphgraph_getfromjs(mock_graph_parentchildsimple_json),
      child_key = '/parent/child',
      child_node = graph_parentchildsimple.get(child_key),
      parent_node = sphfs_getparentnode(
        graph_parentchildsimple, child_node)

  assert.strictEqual(
    parent_node.get('key'), '/parent')
})

let graph_parentchildsimple =
    sphgraph_getfromjs(mock_graph_parentchildsimple_json),
    parent_key = '/parent',
    parent_node = graph_parentchildsimple.get(parent_key)

// eslint-disable-next-line max-len
test("should return the child path '/parent/child' given '/parent' and 'child'", () => {
  assert.strictEqual(
    sphfs_getchildkey(parent_node, 'child'), '/parent/child')
})

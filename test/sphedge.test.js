// Filename: spectraph.edge.spec.js
// Timestamp: 2017.02.24-15:04:59 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import test from 'node:test'
import assert from 'node:assert/strict'

import {
  sphedge_get,
  sphedge_getfromjs,
  sphedge_isvalidname,
  sphedge_issame,
  sphedge_issamenot
} from '../src/sphedge.js'

test('should return an edge node with given key', () => {
  const edge = sphedge_get('id', 'refname', 'edgetype', 'keyval')

  assert.strictEqual(edge.get('key'), 'keyval')
})

test('should return an edge node from given js', () => {
  const edge = sphedge_getfromjs({
    refname: 'refname',
    'gn:type': 'edgetype',
    key: 'edgekey',
    meta: { ismeta: true }
  })

  assert.strictEqual(edge.get('key'), 'edgekey')
  assert.strictEqual(edge.get('refname'), 'refname')
  assert.strictEqual(edge.get('gn:type'), 'edgetype')
  assert.strictEqual(edge.get('meta').ismeta, true)
})

test('should return `true` for a number', () => {
  assert.strictEqual(
    sphedge_isvalidname(-1), true)

  assert.strictEqual(
    sphedge_isvalidname(0), true)

  assert.strictEqual(
    sphedge_isvalidname(1), true)
})

test('should return `true` for a string of length', () => {
  assert.strictEqual(
    sphedge_isvalidname('edgename'), true)
})

test('should return `false` for a string of no length', () => {
  assert.strictEqual(
    sphedge_isvalidname(''), false)
})

test('should return `false` for a value not number or string', () => {
  assert.strictEqual(
    sphedge_isvalidname({}), false)

  assert.strictEqual(
    sphedge_isvalidname(new Date()), false)

  assert.strictEqual(
    sphedge_isvalidname(), false)

  assert.strictEqual(
    sphedge_isvalidname(null), false)
})

const edgea = sphedge_getfromjs({
  id: '1',
  refname: 'refname',
  'gn:type': 'edgetype',
  key: 'edgekey',
  meta: { ismeta: true }
})

const edgeb = sphedge_getfromjs({
  id: '2',
  refname: 'refname',
  'gn:type': 'edgetype',
  key: 'edgekey',
  meta: { ismeta: true }
})

test('should return `true` for a edges which are the same', () => {
  assert.strictEqual(
    sphedge_issame(edgea, edgea), true)
})

test('should return `false` for a different edges with matching values', () => {
  assert.strictEqual(
    sphedge_issame(edgea, edgeb), false)
  assert.strictEqual(
    sphedge_issamenot(edgea, edgeb), true)
})

// Filename: spectraph.graph.spec.js
// Timestamp: 2017.02.25-01:10:06 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import util from 'node:util'
import test from 'node:test'
import assert from 'node:assert/strict'
import { is, List } from 'immutable'

import {
  sphnode_get
} from '../src/sphnode.js'

import {
  sphgraph_get,
  sphgraph_getfromjs,
  sphgraph_setnodechild,
  sphgraph_setnode,
  // sphgraph_getnode,
  sphgraph_getnoderoot,
  // sphgraph_getedgeid,
  sphgraph_setnodeedge,
  sphgraph_rmedgenode,
  sphgraph_setnodeinedge,
  sphgraph_setnodeoutedge,
  // sphgraph_getdeparrall,
  // sphgraph_getdeparr,
  // sphgraph_walkedgeslist,
  sphgraph_walkedges,
  sphgraph_followedge,
  sphgraph_rmedges,
  sphgraph_rmnode,
  sphgraph_getparent,
  // sphgraph_getsiblingkeyprev,
  sphgraph_getsiblingprev,
  // sphgraph_getsiblingkeynext,
  // sphgraph_getsiblingnext,
  sphgraph_dokeychildslist
  // sphgraph_dokeychilds,
  // sphgraph_findkeychild
} from '../src/sphgraph.js'

import {
  mock_graph_todo_json,
  mock_graph_edgenodes_one_to_three_json,
  mock_graph_edgenodes_three_json
} from './mock/mockspecs.js'

test('should return an empty object', () => {
  assert.strictEqual(
    typeof sphgraph_get() === 'object', true)
})

test('should add the given node to the given graph', () => {
  let node = sphnode_get({}, 'name'),
      graph = sphgraph_get()

  ;[graph, node] = sphgraph_setnode(graph, node)

  assert.strictEqual(
    is(graph.get(node.get('key')), node), true)
})

test('should child the given cnode to the given pnode', () => {
  let graph = sphgraph_get(),
      pnode = sphnode_get({}, 'pnode'),
      cnode = sphnode_get({}, 'cname')

  ;[graph, pnode] = sphgraph_setnode(graph, pnode)
  ;[graph, pnode, cnode] =
    sphgraph_setnodechild(graph, pnode, cnode)

  assert.strictEqual(
    typeof graph.get('pnode/cname'), 'object')

  assert.strictEqual(
    pnode.get('childarr').get(0), 'pnode/cname')
})

test('should return a node with a root path, "/pnode"', () => {
  let graph = sphgraph_get(),
      pnode = sphnode_get({}, '/pnode'),
      cnode = sphnode_get({}, 'cname')

  ;[graph, pnode] = sphgraph_setnode(graph, pnode)
  ;[graph, pnode, cnode] =
    sphgraph_setnodechild(graph, pnode, cnode)

  assert.strictEqual(
    sphgraph_getnoderoot(graph).get('key'), '/pnode')
})

test('should return a null if no root path', () => {
  let graph = sphgraph_get(),
      pnode = sphnode_get({}, 'pnode')

  ;[graph, pnode] = sphgraph_setnode(graph, pnode)

  assert.strictEqual(
    sphgraph_getnoderoot(graph), null)
})

test('should create an edge between two nodes', () => {
  let graph = sphgraph_get(),
      innode = sphnode_get({}, 'innode'),
      outnode = sphnode_get({}, 'outname'),
      inedge,
      outedge,

      // refname is name used to identify the out node property from in node
      // which may be variable
      refname = 'refname',
      // need more info
      type = 'specialedgetype',
      ns = 'SUBJ',

      // need more info
      meta = {
        name: '',
        fullstr: ''
      }

  ;[graph, innode] = sphgraph_setnode(graph, innode)
  ;[graph, innode, outnode] =
    sphgraph_setnodechild(graph, innode, outnode)
  ;[graph, innode, outnode] = sphgraph_setnodeedge(
    graph, innode, outnode, refname, type, ns, meta)

  outedge = innode.get('dataoutarr').get(0)
  inedge = outnode.get('datainarr').get(0)

  assert.strictEqual(outedge.get('gn:type'), type)
  assert.strictEqual(outedge.get('refname'), refname)
  assert.strictEqual(outedge.get('key'), 'innode/outname')

  assert.strictEqual(inedge.get('gn:type'), type)
  assert.strictEqual(inedge.get('refname'), refname)
  assert.strictEqual(inedge.get('key'), 'innode')
})

test('should create a self-referencing edge, same node', () => {
  let graph = sphgraph_get(),
      innode = sphnode_get({}, 'innode'),
      inedge,
      outedge,
      refname = 'refname',
      // need more info
      type = 'specialedgetype',
      ns = 'SUBJ',

      // need more info
      meta = {
        name: '',
        fullstr: ''
      }

  ;[graph, innode] = sphgraph_setnode(graph, innode)
  ;[graph, innode, innode] = sphgraph_setnodeedge(
    graph, innode, innode, refname, type, ns, meta)

  outedge = innode.get('dataoutarr').get(0)
  inedge = innode.get('datainarr').get(0)

  assert.strictEqual(outedge.get('gn:type'), type)
  assert.strictEqual(outedge.get('refname'), refname)
  assert.strictEqual(outedge.get('key'), 'innode')

  assert.strictEqual(inedge.get('gn:type'), type)
  assert.strictEqual(inedge.get('refname'), refname)
  assert.strictEqual(inedge.get('key'), 'innode')
})

test('setnodeedge should use given "outnode" and not stale from graph', () => {
  let graph = sphgraph_get(),
      innode = sphnode_get({}, 'innode'),
      outnode = sphnode_get({}, 'outname'),
      inedge,
      outedge,

      // refname is name used to identify the out node property from in node
      // which may be variable
      refname = 'refname',
      // need more info
      type = 'specialedgetype',
      ns = 'SUBJ',

      // need more info
      meta = {
        name: '',
        fullstr: ''
      }

  ;[graph, innode] = sphgraph_setnode(graph, innode)
  ;[graph, innode, outnode] =
    sphgraph_setnodechild(graph, innode, outnode)

  innode = innode.set('new', 'value')
  outnode = outnode.set('new', 'value')
  ;[graph, innode, outnode] = sphgraph_setnodeedge(
    graph, innode, outnode, refname, type, ns, meta)

  outedge = innode.get('dataoutarr').get(0)
  inedge = outnode.get('datainarr').get(0)

  assert.strictEqual(innode.get('new'), 'value')
  assert.strictEqual(outnode.get('new'), 'value')

  assert.strictEqual(outedge.get('gn:type'), type)
  assert.strictEqual(outedge.get('refname'), refname)
  assert.strictEqual(outedge.get('key'), 'innode/outname')

  assert.strictEqual(inedge.get('gn:type'), type)
  assert.strictEqual(inedge.get('refname'), refname)
  assert.strictEqual(inedge.get('key'), 'innode')
})

// eslint-disable-next-line max-len
test('should throw an error if an edge node is not found in graph', async () => {
  let graph = sphgraph_get(),
      innode = sphnode_get({}, 'innode'),
      outnode = sphnode_get({}, 'outname'),
      refname = 'refname',
      // need more info
      type = 'specialedgetype',
      ns = 'SUBJ',

      // need more info
      meta = {
        name: '',
        fullstr: ''
      }

  ;[graph, innode] = sphgraph_setnode(graph, innode)

  await assert.rejects(async () => (
    sphgraph_setnodeedge(graph, innode, outnode, refname, type, ns, meta)
  ), {
    message: '[!!!] edge node not found in graph: outname'
  })
})

test('should remove a node from edge "refname" relationship', () => {
  let graph = sphgraph_get(),
      bgnnode = sphnode_get({}, 'bgnnode'),
      middlenode = sphnode_get({}, 'midnode'),
      endnode = sphnode_get({}, 'endnode'),
      refname = 'refname',
      // need more info
      type = 'specialedgetype',
      ns = 'SUBJ',

      // need more info
      meta = {
        name: 'mytaxes',
        fullstr: '[/datatax].subj.jennytaxes'
      }

  ;[graph, bgnnode] = sphgraph_setnode(graph, bgnnode)
  ;[graph, middlenode] = sphgraph_setnode(graph, middlenode)
  ;[graph, endnode] = sphgraph_setnode(graph, endnode)

  ;[graph, bgnnode, middlenode]
    = sphgraph_setnodeedge(graph, bgnnode, middlenode, refname, type, ns, meta)
  ;[graph, middlenode, endnode]
    = sphgraph_setnodeedge(graph, middlenode, endnode, refname, type, ns, meta)

  const bgnnodewithedges = graph.get(bgnnode.get('key')).toJS()
  const midnodewithedges = graph.get(middlenode.get('key')).toJS()
  const endnodewithedges = graph.get(endnode.get('key')).toJS()

  assert.deepStrictEqual({
    datainarr: bgnnodewithedges.datainarr,
    dataoutarr: bgnnodewithedges.dataoutarr
  }, {
    datainarr: undefined,
    dataoutarr: [{
      'gn:type': 'specialedgetype',
      id: '[midnode].SUBJ.mytaxes ⸺> [/datatax].subj.jennytaxes',
      key: 'midnode',
      meta: bgnnodewithedges.dataoutarr[0].meta,
      ns: 'SUBJ',
      refname: 'refname'
    }]
  })

  assert.deepStrictEqual({
    datainarr: midnodewithedges.datainarr,
    dataoutarr: midnodewithedges.dataoutarr
  }, {
    datainarr: [{
      'gn:type': 'specialedgetype',
      id: '[midnode].SUBJ.mytaxes ⸺> [/datatax].subj.jennytaxes',
      key: 'bgnnode',
      refname: 'refname'
    }],
    dataoutarr: [{
      'gn:type': 'specialedgetype',
      id: '[endnode].SUBJ.mytaxes ⸺> [/datatax].subj.jennytaxes',
      key: 'endnode',
      meta: midnodewithedges.dataoutarr[0].meta,
      ns: 'SUBJ',
      refname: 'refname'
    }]
  })

  assert.deepStrictEqual({
    datainarr: endnodewithedges.datainarr,
    dataoutarr: endnodewithedges.dataoutarr
  }, {
    datainarr: [{
      'gn:type': 'specialedgetype',
      id: '[endnode].SUBJ.mytaxes ⸺> [/datatax].subj.jennytaxes',
      key: 'midnode',
      refname: 'refname'
    }],
    dataoutarr: undefined
  })

  ;[graph] = sphgraph_rmedgenode(graph, middlenode, refname)


  const bgnnodewithedgesremoved = graph.get(bgnnode.get('key')).toJS()
  const midnodewithedgesremoved = graph.get(middlenode.get('key')).toJS()
  const endnodewithedgesremoved = graph.get(endnode.get('key')).toJS()
  assert.deepStrictEqual({
    datainarr: bgnnodewithedgesremoved.datainarr,
    dataoutarr: bgnnodewithedgesremoved.dataoutarr
  }, {
    datainarr: undefined,
    dataoutarr: [{
      'gn:type': 'specialedgetype',
      id: '[endnode].SUBJ.mytaxes ⸺> [/datatax].subj.jennytaxes',
      key: 'endnode',
      meta: {
        fullstr: '[/datatax].subj.jennytaxes',
        name: 'mytaxes'
      },
      ns: 'SUBJ',
      refname: 'refname'
    }]
  })

  assert.deepStrictEqual({
    datainarr: midnodewithedgesremoved.datainarr,
    dataoutarr: midnodewithedgesremoved.dataoutarr
  }, {
    datainarr: [],
    dataoutarr: []
  })

  assert.deepStrictEqual({
    datainarr: endnodewithedgesremoved.datainarr,
    dataoutarr: endnodewithedgesremoved.dataoutarr
  }, {
    datainarr: [{
      'gn:type': 'specialedgetype',
      id: '[endnode].SUBJ.mytaxes ⸺> [/datatax].subj.jennytaxes',
      key: 'bgnnode',
      refname: 'refname'
    }],
    dataoutarr: []
  })
})

test('should remove node w/ one sibling from edge "refname" relship', () => {
  let graph = sphgraph_get(),
      bgnnode = sphnode_get({}, 'bgnname'),
      middlenode = sphnode_get({}, 'middlename'),
      outedge,

      refname = 'refname',
      // need more info
      type = 'specialedgetype',
      ns = 'SUBJ',

      // need more info
      meta = {
        name: 'mytaxes',
        fullstr: '[/datatax].subj.jennytaxes'
      }

  ;[graph, bgnnode] = sphgraph_setnode(graph, bgnnode)
  ;[graph, middlenode] = sphgraph_setnode(graph, middlenode)

  ;[graph, bgnnode, middlenode] = sphgraph_setnodeedge(
    graph, bgnnode, middlenode, refname, type, ns, meta)

  ;[graph, middlenode]
    = sphgraph_rmedgenode(graph, middlenode, refname)

  bgnnode = graph.get(bgnnode.get('key'))
  outedge = bgnnode.get('dataoutarr').get(0)

  assert.strictEqual(outedge, undefined)
})

test('should insert "node" as "in" edge to edgenode', () => {
  let graph = sphgraph_get(),
      bgnnode = sphnode_get({}, 'bgnname'),
      middlenode = sphnode_get({}, 'middlename'),
      endnode = sphnode_get({}, 'endname'),
      bgnoutedge,
      middleoutedge,

      refname = 'refname',
      // need more info
      type = 'specialedgetype',
      ns = 'SUBJ',

      // need more info
      meta = {
        name: 'mytaxes',
        fullstr: '[/datatax].subj.jennytaxes'
      }

  ;[graph, bgnnode] = sphgraph_setnode(graph, bgnnode)
  ;[graph, middlenode] = sphgraph_setnode(graph, middlenode)
  ;[graph, endnode] = sphgraph_setnode(graph, endnode)

  ;[graph, bgnnode, endnode] =
    sphgraph_setnodeedge(graph, bgnnode, endnode, refname, type, ns, meta)

  ;[graph, bgnnode] =
    sphgraph_setnodeinedge(graph, bgnnode, refname, middlenode)

  bgnnode = graph.get(bgnnode.get('key'))
  middlenode = graph.get(middlenode.get('key'))
  endnode = graph.get(endnode.get('key'))

  bgnoutedge = bgnnode.get('dataoutarr').get(0)

  assert.strictEqual(bgnoutedge.get('refname'), refname)
  assert.strictEqual(bgnoutedge.get('key'), 'middlename')

  middleoutedge = middlenode.get('dataoutarr').get(0)

  assert.strictEqual(middleoutedge.get('refname'), refname)
  assert.strictEqual(middleoutedge.get('key'), 'endname')
})

test('should insert "node" as "out" edge to edgenode', () => {
  let graph = sphgraph_get(),
      bgnnode = sphnode_get({}, 'bgnname'),
      middlenode = sphnode_get({}, 'middlename'),
      endnode = sphnode_get({}, 'endname'),
      middleinedge,
      endinedge,

      refname = 'refname',
      // need more info
      type = 'specialedgetype',
      ns = 'SUBJ',

      // need more info
      meta = {
        name: 'mytaxes',
        fullstr: '[/datatax].subj.jennytaxes'
      }

  ;[graph, bgnnode] = sphgraph_setnode(graph, bgnnode)
  ;[graph, middlenode] = sphgraph_setnode(graph, middlenode)
  ;[graph, endnode] = sphgraph_setnode(graph, endnode)

  ;[graph, bgnnode, endnode] = sphgraph_setnodeedge(
    graph, bgnnode, endnode, refname, type, ns, meta)

  ;[graph, endnode] =
    sphgraph_setnodeoutedge(graph, endnode, refname, middlenode)

  bgnnode = graph.get(bgnnode.get('key'))
  middlenode = graph.get(middlenode.get('key'))
  endnode = graph.get(endnode.get('key'))

  endinedge = endnode.get('datainarr').get(0)

  assert.strictEqual(endinedge.get('refname'), refname)
  assert.strictEqual(endinedge.get('key'), 'middlename')

  middleinedge = middlenode.get('datainarr').get(0)

  assert.strictEqual(middleinedge.get('refname'), refname)
  assert.strictEqual(middleinedge.get('key'), 'bgnname')
})

// eslint-disable-next-line max-len
test('should walk one level of "edgename" "out" nodes starting from srcnode', async () => {
  let graph = sphgraph_getfromjs(mock_graph_edgenodes_three_json),
      nodenames = []

  const res = await new Promise(resolve => sphgraph_walkedges(
    graph, graph.get('anode'), 'specialedge', (graph, srcnode, edge, fn) => {
      nodenames.push(edge.get('key'))

      fn(null, graph)
    }, () => {
      assert.strictEqual(nodenames.join(''), 'bname')

      resolve(true)
    }))

  assert.strictEqual(res, true)
})

test('should follow "edgename" "out" nodes starting from srcnode', async () => {
  let graph = sphgraph_getfromjs(mock_graph_edgenodes_three_json),
      nodenames = []

  const res = await new Promise(resolve => {
    sphgraph_followedge(graph, graph.get('anode'), 'specialedge', () => {
      assert.strictEqual(nodenames.join('+'), 'bname+cname')

      resolve(true)
    }, (graph, edgenode, edge, fn) => {
      nodenames.push(edge.get('key'))

      fn(null, graph, edgenode)
    })
  })

  assert.strictEqual(res, true)
})

test('should remove all "in" edges from node', () => {
  let graph = sphgraph_getfromjs(mock_graph_edgenodes_one_to_three_json),
      anode = graph.get('anode')

  ;[graph, anode] = sphgraph_rmedges(graph, anode)

  assert.strictEqual(anode.get('datainarr').count(), 0)

  assert.strictEqual(graph.get('bname').get('dataoutarr').count(), 0)
  assert.strictEqual(graph.get('cname').get('dataoutarr').count(), 0)
  assert.strictEqual(graph.get('dname').get('dataoutarr').count(), 0)
})

test('should remove the node and all "in" edges from graph', () => {
  let graph = sphgraph_getfromjs(mock_graph_edgenodes_one_to_three_json),
      anode = graph.get('anode')

  ;[graph, anode] = sphgraph_rmnode(graph, anode)

  assert.strictEqual(graph.get(anode.get('key')), undefined)

  assert.strictEqual(graph.get('bname').get('dataoutarr').count(), 0)
  assert.strictEqual(graph.get('cname').get('dataoutarr').count(), 0)
  assert.strictEqual(graph.get('dname').get('dataoutarr').count(), 0)
})

let graph_todo = sphgraph_getfromjs(mock_graph_todo_json),
    listusers_key = '/todo/content/box/listusers',
    listusers_node = graph_todo.get(listusers_key)

test('should return parent node', () => {
  let pnode = sphgraph_getparent(graph_todo, listusers_node)

  assert.strictEqual(pnode.get('key'), '/todo/content/box')
})

test('should return previous sibling', () => {
  let graph_todo = sphgraph_getfromjs(mock_graph_todo_json),
      user1_key = '/todo/content/datausers/id1',
      user2_key = '/todo/content/datausers/id2',
      // user1_node = graph_todo.get(user1_key),
      user2_node = graph_todo.get(user2_key),

      node_sibling = sphgraph_getsiblingprev(graph_todo, user2_node)

  assert.strictEqual(node_sibling.get('key'), user1_key)
})

test('should return null if !previous sibling', () => {
  let graph_todo = sphgraph_getfromjs(mock_graph_todo_json),
      user1_key = '/todo/content/datausers/id1',
      // user2_key = '/todo/content/datausers/id2',
      user1_node = graph_todo.get(user1_key),
      // user2_node = graph_todo.get(user2_key),

      node_sibling = sphgraph_getsiblingprev(graph_todo, user1_node)

  assert.strictEqual(node_sibling, null)
})

test('dokeychildslist should do to child keys', async () => {
  const graph_todo = sphgraph_getfromjs(mock_graph_todo_json)
  const graph_updated = await util.promisify(sphgraph_dokeychildslist)(
    graph_todo, List([
      '/todo/content/datausers/id1',
      '/todo/content/datausers/id2'
    ]), (gr, keych, i, fn) => {
      fn(null, gr, gr.get(keych), true)
    })

  assert.ok(graph_updated)
})

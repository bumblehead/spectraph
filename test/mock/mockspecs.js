import fs from 'node:fs'
import url from 'node:url'
import path from 'node:path'

const [
  mock_graph_todo_json,
  mock_graph_canvas_img_thumb_json,
  mock_graph_edgenodes_one_to_three_json,
  mock_graph_edgenodes_three_json,
  mock_graph_parentchildsimple_json,
  mock_graph_parentchildstwo_json
] = [
  './graph_todo.json',
  './graph_canvas_img_thumb.json',
  './graph_edgenodes_one_to_three.json',
  './graph_edgenodes_three.json',
  './graph_parentchildsimple.json',
  './graph_parentchildstwo.json'
].map(name => JSON.parse(
  fs.readFileSync(
    path.join(url.fileURLToPath(new url.URL('.', import.meta.url)), name))))

export {
  mock_graph_todo_json,
  mock_graph_canvas_img_thumb_json,
  mock_graph_edgenodes_one_to_three_json,
  mock_graph_edgenodes_three_json,
  mock_graph_parentchildsimple_json,
  mock_graph_parentchildstwo_json
}

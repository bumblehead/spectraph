// Filename: spectraph.node.spec.js
// Timestamp: 2017.02.18-01:58:34 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import test from 'node:test'
import assert from 'node:assert/strict'

import {
  sphnode_is,
  sphnode_get,
  sphnode_getas,
  sphnode_issame,
  sphnode_assign,
  sphnode_setedgeref,
  sphnode_getdatainarr
} from '../src/sphnode.js'

import {
  sphgraph_get,
  sphgraph_setnode,
  sphgraph_setnodechild
} from '../src/sphgraph.js'

test('should return a node with given filepath and content', () => {
  const node = sphnode_get({}, 'keyval')

  assert.ok(sphnode_is(node))
  assert.ok(sphnode_issame(node, node))
  assert.strictEqual(node.get('key'),  'keyval')
})

test('getdatainarr should return datainarr list', () => {
  const node = sphnode_get({
    datainarr: [{
      refname: 'thumb',
      key: 'canvas/nav/links/thumb'
    }, {
      refname: 'img',
      key: 'canvas/nav/links/img'
    }]
  }, 'canvas/nav')

  assert.deepStrictEqual(
    sphnode_getdatainarr(node), [{
      refname: 'thumb',
      key: 'canvas/nav/links/thumb'
    }, {
      refname: 'img',
      key: 'canvas/nav/links/img'
    }]
  )
})

test('getas should return a new node from the given node', () => {
  const node = sphnode_get({
    datainarr: [{
      refname: 'thumb',
      key: 'canvas/nav/links/thumb'
    }, {
      refname: 'img',
      key: 'canvas/nav/links/img'
    }]
  }, 'canvas/nav')

  const node_another = sphnode_getas(node, 'a new name')

  assert.deepStrictEqual(node_another.toJS(), {
    name: 'a new name',
    key: 'a-new-name',
    datainarr: [
      { refname: 'thumb', key: 'canvas/nav/links/thumb' },
      { refname: 'img', key: 'canvas/nav/links/img' }
    ]
  })
})

test('assign should return a new node with assigned values', () => {
  const node = sphnode_get({
    datainarr: [{
      refname: 'thumb',
      key: 'canvas/nav/links/thumb'
    }, {
      refname: 'img',
      key: 'canvas/nav/links/img'
    }]
  }, 'canvas/nav')

  const node_another = sphnode_assign(node, {
    datainarr: []
  })

  assert.deepStrictEqual(node_another.toJS(), {
    key: 'canvas/nav',
    name: 'canvas/nav',
    datainarr: []
  })
})

test('setedgeref should set the edge of a new node ', () => {
  const node = sphnode_get({
    datainarr: [{
      refname: 'thumb',
      key: 'canvas/nav/links/thumb'
    }, {
      refname: 'img',
      key: 'canvas/nav/links/img'
    }]
  }, 'canvas/nav')

  const node_another = sphnode_setedgeref(
    node, 'key', 'refname', 'edgename', 'meta')

  assert.deepStrictEqual(node_another.toJS(), {
    name: 'canvas/nav',
    key: 'canvas/nav',
    datainarr: [
      { refname: 'thumb', key: 'canvas/nav/links/thumb' },
      { refname: 'img', key: 'canvas/nav/links/img' }
    ],
    edgename: [{
      id: 'refname',
      'gn:type': 'key',
      refname: 'key',
      key: undefined,
      meta: 'meta'
    }]
  })
})

test('should add a node to a spectree node', () => {
  // when added to root, begin slash is added to key
  // regardless it is included in name or not
  let pnode = sphnode_get({}, '/canvas'),
      cnode = sphnode_get({}, 'nav'),
      [graph, pnodenew] = sphgraph_setnode(sphgraph_get(), pnode),
      [graphfin/*,cnodefin,pnodefin*/] = sphgraph_setnodechild(
        graph, pnodenew, cnode)

  assert.strictEqual(graphfin.get('/canvas/nav').get('name'), 'nav')
  assert.strictEqual(graphfin.get('/canvas').get('name'), '/canvas')
  // eslint-disable-next-line max-len
  assert.strictEqual(graphfin.get('/canvas').get('childarr').get(0), '/canvas/nav')
})

/*
 test('should add a node to a spectree node to build a tree', function () {
    var traph = sphgraph_getfromjs({
      'canvas': {
        'key': 'canvas',
        'name': 'canvas',
        'spec': {},
        'childarr': [
          'canvas/nav'
        ],
        'inarr': [],
        'outarr': []
      },
      'canvas/nav': {
        'key': 'canvas/nav',
        'name': 'nav',
        'spec': {},
        'childarr': [
          'canvas/nav/links'
        ],
        'inarr': [],
        'outarr': []
      },
      'canvas/nav/links': {
        'key': 'canvas/nav/links',
        'name': 'links',
        'spec': {},
        'childarr': [
          'canvas/nav/links/img',
          'canvas/nav/links/thumb'
        ],
        'inarr': [],
        'outarr': []
      },
      'canvas/nav/links/img': {
        'key': 'canvas/nav/links/img',
        'name': 'img',
        'spec': {},
        'childarr': [],
        'inarr': [],
        'outarr': []
      },
      'canvas/nav/links/thumb': {
        'key': 'canvas/nav/links/thumb',
        'name': 'thumb',
        'spec': {},
        'childarr': [],
        'inarr': [],
        'outarr': []
      }
    });

    var tree = spectraph.tree.getfromchild(traph);

    assert.strictEqual(
      JSON.stringify(tree)
    ).toBe(
      JSON.stringify({
        'key': 'canvas',
        'childarr': [
          {
            'key': 'canvas/nav',
            'childarr': [
              {
                'key': 'canvas/nav/links',
                'childarr': [
                  {
                    'key': 'canvas/nav/links/img',
                    'childarr': []
                  },
                  {
                    'key': 'canvas/nav/links/thumb',
                    'childarr': []
                  }
                ]
              }
            ]
          }
        ]
      })
    );
  });
*/



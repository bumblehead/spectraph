spectraph
=========

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)][1]
[![coverage report](https://gitlab.com/bumblehead/spectraph/badges/main/coverage.svg)][3]

[0]: http://www.bumblehead.com                            "bumblehead"
[1]: https://www.gnu.org/licenses/gpl-3.0
[3]: https://gitlab.com/bumblehead/spectraph/commits/main



nothing to see yet.

graph for use with a design language. graph is immutable and is favorable to tree construction. sample tree and graph included.

uniformity and consistency over speed.
